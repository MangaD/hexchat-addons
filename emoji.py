"""
	Usage: /emoji <word>
	Creator: MangaD <https://bitbucket.org/MangaD/hexchat-addons>
	Emojis' source: https://www.npmjs.com/package/cool-ascii-faces
"""
__module_name__ = "Emojis"
__module_version__ = "1.0"
__module_description__ = "Gets the emoji and prints it"
import hexchat

dic = {
	"hug":"(づ｡◕‿‿◕｡)づ",
	"meow":"(｡◕‿‿◕｡)",
	"wave":"~(˘▾˘~)",
	"eyes":"=^.^=",
	"salute":"(•◡•)/",
	"sunglasses":"(⌐■_■)",
	"proud":"(ღ˘⌣˘ღ)",
	"fight":"(ง •̀_•́)ง",
	"lenny": "(͡° ͜ʖ ͡°)",
	"perv": "(¬‿¬)",
	"bigeyes": "(ʘ‿ʘ)",
	"no": "( ︶︿︶)",
	"shrug": "¯\_(ツ)_/¯",
	"bear": "ʕ•ᴥ•ʔ",
	"wink": "◕‿↼",
	"kiss": "( ˘ ³˘)♥"
}

def emoji(word, word_eol, userdata):
	try:
		hexchat.prnt(dic[word_eol[1]])
	except:
		hexchat.prnt('Emoji: Not found.')

hexchat.hook_command('emoji', emoji, help='EMOJI <word>')
